#!/bin/bash
# This is the BuildGrid start script

config=${STRESS_TESTING_BGD_CONFIG}
if [[ -z "$config" ]]; then
    echo "STRESS_TESTING_BGD_CONFIG not specified; using default"
    config="bgd-sqlite.yml"
fi

echo "Starting buildgrid with config: [$config]"

config=${STRESS_TESTING_BGD_CONFIG:-bgd-sqlite.yml}

/app/env/bin/bgd server start -vv /mnt/configs/$config

