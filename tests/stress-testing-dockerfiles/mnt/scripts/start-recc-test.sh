#!/bin/bash
# This is the bash script that makes some RE requests using recc

cwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


if [[ -z "${STRESS_TESTING_DISABLE_PLATFORM_PROPERTIES}" ]]; then
    # Pick some of the few OSFamily/ISAs
    export RECC_REMOTE_PLATFORM_OSFamily=$("$cwd/get-random-osfamily.sh")
    export RECC_REMOTE_PLATFORM_ISA=$("$cwd/get-random-isa.sh")
    env | grep RECC # Log those
fi


# This script runs ST_ITER jobs. (where ST_ITER is an environment variable)
# Defaults to 1 iteration if not set.

# Before each job, it will sleep s for [0,5] seconds and
# then copy an uploaded file containing a random number
# to the expected output location.
# It will then check whether the downloaded result.txt matches the input
# Exit codes:
#   0: Worker successfully ran the command and results match
#   2: The worker returned the wrong result
#   Anything else: Other errors (inspect)
#

# Things being tested:
#   * (recc<->bgd-cas)  GetActionResult
#   * (recc<->bgd-cas) FindMissingBlobs
#   * (recc<->bgd-cas<->bots) BatchUpdateBlobs
#   * (recc<->bgd-cas<->bots) BatchReadBlobs
#   * (recc<->bgd-exec) WaitExecution
#   * (recc<->bgd-cas<->bots) Cas Upload
#   * (recc<->bgd-cas<->bots) Cas Downloads
#   * (bgd-exec<->bots) UpdateActionResult

iter=${STRESS_TESTING_CLIENT_REQUESTS:-1}

for ((i=1;i<=iter;i++)); do
    echo "Iteration [$i/$iter]"
    sleep $[ ( $RANDOM % 5 ) ]s

    mynum=$RANDOM
    echo $mynum > expected.txt

    set -o pipefail

    RECC_DEPS_OVERRIDE=expected.txt \
    RECC_OUTPUT_FILES_OVERRIDE=result.txt \
        recc cp expected.txt result.txt
    exitcode=$?
    if [[ $exitcode -ne 0 ]]; then
        exit $exitcode
    fi

    diff -y result.txt expected.txt || exit 2
done

