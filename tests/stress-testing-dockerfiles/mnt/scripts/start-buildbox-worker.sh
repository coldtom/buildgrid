#!/bin/bash
# This is the buildbox-worker start script

cwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

additional_args=""
if [[ -z "${STRESS_TESTING_DISABLE_PLATFORM_PROPERTIES}" ]]; then
    # Pick some of the few OSFamily/ISAs
    OSFamily=$("$cwd/get-random-osfamily.sh")
    ISA=$("$cwd/get-random-isa.sh")

    echo "Starting worker with: OSFamily: [$OSFamily], ISA: [$ISA]"
    additional_args="--platform OSFamily=$OSFamily --platform ISA=$ISA"
else
    echo "Platform Properties are disabled."
fi

buildbox-worker --verbose \
    $additional_args \
    --buildbox-run=buildbox-run-hosttools \
    --bots-remote=http://buildgrid:50051 \
    --cas-remote=http://buildgrid:50051 \
    --request-timeout=10

