#!/bin/bash

docker_compose_image="${STRESS_TESTING_DOCKER_COMPOSE:-docker-compose-pges-recc-bbrht.yml}"

docker-compose -f "$docker_compose_image" ps | grep -v "Exit 0"

